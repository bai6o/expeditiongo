from func import *
import time
import pygame

mid = [400, 300]        # mid of ship base
height = 30             # height
vel = 0                 # speed
lifes = 3               # number of lifes ramining

len_bullet = 15
vel_bullet = 15
max_bullets = 25

width_screen = 800
height_screen = 600
teste="false"
flag=True
def main():
  # initialize pygame module
  pygame.init()

  # create a display of size 800 x 600
  screen = pygame.display.set_mode((width_screen, height_screen))
  bg = pygame.transform.scale(pygame.image.load("pics/bg.png"), (width_screen, height_screen))

  shot_sound = pygame.mixer.Sound("sounds/shot.wav")

  # images
  meteor_img1 = pygame.image.load("pics/meteor1.png")
  meteor_img2 = pygame.image.load("pics/meteor2.png")
  satellite_img = pygame.image.load("pics/satellite.png")
  heart_img = pygame.transform.scale(pygame.image.load("pics/heart.png"),(35, 35))

  # fonts
  myfont = pygame.font.SysFont("Comic Sans MS", 30)
  H_fonte = pygame.font.Font("fonte/Kenney_Future_Narrow.ttf",30)

  # generate ship
  ship = Ship(mid, height, vel, lifes, (255,0,0))

  clock = pygame.time.Clock()

  invincible = False
  game_over = False
  xx = False

  score = 0
  count = 60

  # game state variable
  # 0->game 1->pause 2->dead 3->Satelite 4 -> menu
  gs = 4
  print(gs)
  bullets = []
  asteroids = []
  satellites = []
  new_lifes = []

  pygame.mixer.music.load("sounds/sound_track.wav")
  pygame.mixer.music.play(-1)
  pygame.mixer.music.set_volume(0.1)

  # main loop
  while not game_over:
    # mouse current position
    cur = pygame.mouse.get_pos()

    # set bg
    screen.blit(bg, (0,-2))

    # game state
    if gs == 0:

      dt = clock.tick()

      if pygame.mouse.get_pressed()[0]==1:
          print(pygame.mouse.get_rel())
          ship.move_ship(cur, screen)
          
      # event handling, gets all events from the event queue
      for event in pygame.event.get():
        # if quiting the game
        if event.type == pygame.QUIT:
          game_over = True

        if event.type == pygame.KEYDOWN:
          if event.key == pygame.K_SPACE:
            # play shooting sound
            pygame.mixer.Sound.play(shot_sound)

            # generate new bullet every time space is pressed and add it to bullet list
            new_bullet = Bullet(len_bullet, vel_bullet)
            new_bullet.gen_bullet(ship, cur, screen)

            bullets.append(new_bullet)
          if event.key == pygame.K_p:
            gs = 1

      # draw
      if not invincible:
        ship.draw_ship(cur, screen)
      else:
        count-=1
        if xx:
          ship.draw_ship(cur, screen)
        xx = not xx
        if count == 0:
          count = 60
          invincible = False

      score_label = myfont.render("score: "+str(score), 3, (255, 255, 0))
      screen.blit(score_label, (10, 10))

      if ship.v>0:
        ship.v-=1

      # generate asteroid
      if random.randint(0, 100) < 7:
        meteor_img = meteor_img1
        if random.randint(0,10)%2==0:
          meteor_img = meteor_img2
        radius = random.randint(15, 40)
        asteroid = Object(width_screen, height_screen,\
                    radius, random.randint(2, 8),\
                    pygame.transform.scale(meteor_img, (radius*2, radius*2)))

        asteroids.append(asteroid)

      # generate satellite
      if random.randint(0, 300) == 0:
        satellites.append(Object(width_screen, height_screen,\
                            15, random.randint(2, 8),\
                            pygame.transform.scale(satellite_img, (30, 30)) ))

      # generate new life
      if random.randint(0, 1000) <= 1:
        print("new life generated")
        new_lifes.append(Object(width_screen, height_screen,\
                          15, random.randint(2,8),\
                          pygame.transform.scale(heart_img,(30, 30))))

      if len(bullets) > max_bullets:
        bullets.clear()

      # update every life status
      for l in new_lifes:
        l.update_obj(screen)
        if l.pos[0] >= width_screen or l.pos[1] >= height_screen:
          new_lifes.remove(l)
        if l.pos[0] <= -10 or l.pos[1] <= -10:
          new_lifes.remove(l)

        if l.detect_colision(ship.points[0]):
         if ship.lifes <= 4:
          ship.lifes += 1
          new_lifes.remove(l)

      # update every bullet status
      for b in bullets:
        b.update_bullet(screen)
        if b.pos[0][0] >= width_screen or b.pos[0][1] >= height_screen:
          bullets.remove(b)

      # update every asteroid status
      for a in asteroids:
        a.update_obj(screen)
        if a.pos[0] >= width_screen or a.pos[1] >= height_screen:
          asteroids.remove(a)
        if a.pos[0] <= 0 or a.pos[1] <= 0:
          asteroids.remove(a)

        for b in bullets:
          if a.detect_colision(b.pos[1]):
            asteroids.remove(a)
            bullets.remove(b)
        if not invincible:
          for p in ship.points:
            if a.detect_colision(p):
              invincible = True
              ship.lifes -= 1
              if ship.lifes <= 0:
                # change game state to dead
                gs = 2
                print("G A M E  O V E R")

      # update every satellite status
      for s in satellites:
        s.update_obj(screen)
        if s.pos[0] >= width_screen or s.pos[1] >= height_screen:
          satellites.remove(s)

        for p in ship.points: # colisão com satelite
          if s.detect_colision(p):
            gs = 3
            flag = True
            print("S A T E L L I T E")
            satellites.remove(s)

      display_hearts(ship.lifes, heart_img, screen)

    # pause state
    elif gs == 1:
      pause_label = myfont.render("Pause", 3, (255, 255, 0))
      label1 = myfont.render("Press q to quit", 3, (255, 255, 0))
      label2 = myfont.render("Press p to unpause", 3, (255, 255, 0))

      screen.blit(pause_label, (width_screen/2 - 20, height_screen/2 - 10))
      screen.blit(label1, (width_screen/2 - 60, height_screen/2 + 30))
      screen.blit(label2, (width_screen/2 - 80, height_screen/2 + 50))

      for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
          if event.key == pygame.K_p:
            gs = 0

          if event.key == pygame.K_q:
            game_over = True

    # dead state
    elif gs == 2:
      score_labelV2=myfont.render("Sua Pontuação foi "+ str(score), 3, (255,255, 0))
      dead_label = myfont.render("você morreu", 3, (255, 255, 0))
      label1 = myfont.render("Press q to quit", 3, (255, 255, 0))
      label2 = myfont.render("Press enter to play again", 3, (255, 255, 0))

      screen.blit(dead_label, (width_screen/2 - 45 , height_screen/2 - 10))
      screen.blit(score_labelV2, (width_screen/2 -98, height_screen/2 -50))
      screen.blit(label1, (width_screen/2 - 60, height_screen/2 + 30))
      screen.blit(label2, (width_screen/2 - 80, height_screen/2 + 55))

      for event in pygame.event.get():
        if event.type == pygame.QUIT:
          game_over = True

        if event.type == pygame.KEYDOWN:
          if event.key == pygame.K_RETURN:
            gs = 0
            ship.lifes = 3
            ship.m = mid
            asteroids = []
            bullets = []
            satellites = []
            new_lifes = []
            invincible = False
            teste="false"
            perrepetida="false"
            score=0
            ship.color = (255,0,0)

          if event.key == pygame.K_q:
            game_over = True

    # Satelite State
    elif gs == 3:
           if flag == True:
             per, resposta, perrepetida = perguntas()
             flag = False
             pergunta = myfont.render(per, 3, (255, 255, 0))

           #desenha_tela_satelite(pergunta, screen, height_screen, width_screen,myfont)
           Botao1, Botao2 =tela_satelite(screen, height_screen, width_screen, H_fonte,per)

           for event in pygame.event.get():
             tent = False
             if event.type == pygame.QUIT:
               game_over = True
             if (event.type == pygame.MOUSEBUTTONDOWN) or event.type == pygame.KEYDOWN:
               pos_m = pygame.mouse.get_pos()
               key = pygame.key.get_pressed()
               if (Botao1.collidepoint(pos_m)) or (key[pygame.K_y]):
                 print("Y")
                 aux = True
                 tent = True
               elif (Botao2.collidepoint(pos_m)) or (key[pygame.K_n]):
                 print("N")
                 aux = False
                 tent = True
               else:
                 print("nada")
               if tent:
                 if resposta == aux:
                   res = "ACERTOU!"
                   if teste != "true":
                    score+=50
                    gs=0
                   if teste=="true":
                     tela_satelite(screen, height_screen, width_screen, H_fonte, per, res)
                     """"
                     screen.fill((0, 0, 0))
                     screen.blit(res, (width_screen / 2 - 20, height_screen / 2 + 10))
                     pygame.display.update()
                     time.sleep(2)
                     """
                     time.sleep(2)
                     gs=4
                 else:
                   res = "ERROU :("
                 tela_satelite(screen, height_screen, width_screen, H_fonte, per, res)
                 """"
                 screen.fill((0, 0, 0))
                 screen.blit(res, (width_screen / 2 - 20, height_screen / 2 + 10))
                 pygame.display.update()
                 time.sleep(2)
                 """
                 time.sleep(2)
                 if teste=="true":
                  gs = 4
                 if teste != "true":
                  gs = 0
    # Menu state
    elif gs == 4:
      b_start,b_dead,b_sat,b_saida = desenha_menu(screen, height_screen, width_screen,bg)
      for event in pygame.event.get():
        # if quiting the game
        if event.type == pygame.QUIT:
          game_over = True
        if (event.type == pygame.MOUSEBUTTONDOWN):
          pos_m = pygame.mouse.get_pos()
          if b_start.collidepoint(pos_m):
            perrepetida=[]
            teste="false"
            gs = 0
          elif b_dead.collidepoint(pos_m):
            gs = 2
          elif b_sat.collidepoint(pos_m):
            flag = True
            teste="true"
            gs=3
          elif b_saida.collidepoint(pos_m):
            game_over = True
      #gs = 0


    pygame.display.update()
    clock.tick(30)


if __name__=="__main__":
  main()
