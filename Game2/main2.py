from func2 import *
import time
import pygame

mid = [150, 300]        # mid of ship base
height = 30             # height
vel = 0                 # speed
lifes = 3               # number of lifes ramining
ship_x = 0
ship_y = 0
len_bullet = 15
vel_bullet = 15
max_bullets = 25
ordemlist = []
len_bullet = 15
vel_bullet = 15
max_bullets = 25
pos1x=120
pos2x=200
pos3x=280
pos4x=360
pos5x=440
pos1y=400
pos2y=400
pos3y=400
pos4y=400
pos5y=400
once1=0
once2=0
once3=0
width_screen = 800
x=100
y=100
height_screen = 600
teste="false"
flag=True
def main():
  # initialize pygame module
  pygame.init()

  # create a display of size 800 x 600
  screen = pygame.display.set_mode((width_screen, height_screen))
  #Botei height_screen mais 5 pq sobrava um espaço pequeno preto embaixo
  bg3= pygame.transform.scale(pygame.image.load("pics2/bg.png"), (width_screen, height_screen+5))
  bg2= pygame.transform.scale(pygame.image.load("pics2/scenary2.png"), (width_screen, height_screen+5))
  bg = pygame.transform.scale(pygame.image.load("pics2/starfield.png"), (width_screen, height_screen+5))
  planeta1= pygame.transform.scale(pygame.image.load("pics2/Terra.png"), (125,125))
  #Terraquadro=pygame.transform.scale(pygame.image.load("pics2/Terra.png"), (125,125))
  bluesquare= pygame.transform.scale(pygame.image.load("pics2/bluesquare.png"), (40,40))
  planeta2=pygame.transform.scale(pygame.image.load("pics2/Marte.png"), (75,75))
  starman=pygame.transform.scale(pygame.image.load("pics2/starman.png"),(200,295))
  board= pygame.transform.scale(pygame.image.load("pics2/board.png"),(500,295))
  textbubble=pygame.transform.scale(pygame.image.load("pics2/textbubble.png"),(180,125))
  check=pygame.transform.scale(pygame.image.load("pics2/check.png"),(90,90))
  check2=pygame.transform.scale(pygame.image.load("pics2/check.png"),(52,52))
  heart_img = pygame.transform.scale(pygame.image.load("pics2/heart.png"),(35, 35))
  letters = ['A', 'E', 'R', 'T']
  letter_images = {}
  for l in letters:
      letter_images[l] = pygame.transform.scale(pygame.image.load("pics2/" + l + ".png"),(46,40)).convert_alpha()

  planeta1=planeta1.convert_alpha()
  planeta2=planeta2.convert_alpha()
  planeta1rect= planeta1.get_rect()
  starman=starman.convert_alpha()
  textbubble=textbubble.convert_alpha()
  board=board.convert_alpha()
  bluesquare=bluesquare.convert_alpha()


  # generate ship
  ship = Ship(mid, height, vel, lifes, (255,0,0))
  shot_sound = pygame.mixer.Sound("sounds/shot.wav")

  clock = pygame.time.Clock()
  invincible=False
  game_over = False
  xx = False
  once=0
  once1=0
  teste="false"
  flag=True
  once2=0
  once3=0
  score = 0
  count = 60
  enterlevel = pygame.mixer.Sound('sounds/laser6.wav')
  pos1x=120
  pos2x=200
  pos3x=280
  pos4x=360
  pos5x=440
  pos1y=400
  pos2y=400
  pos3y=400
  pos4y=400
  pos5y=400
  test1=0
  test2=0
  test3=0
  test4=0
  test5=0
  umsó="false"
  firstr=0
  checkstage=0
  checkstage2=0
  highscore1=0
  highscore2=0
  listaposcerta=[]
  listaposcerta2=[]
  once4=0
  once5=0
  checkstage=0
  score1=0
  score2=0
  objetivo=0
  # game state variable
  # 0->game 1->pause 2->dead 3->Satelite 4 -> menu
  timer=300
  bgi=0
  gs = 0
  print(gs)
  bullets = []
  asteroids = []
  satellites = []
  new_lifes = []

  myfont = pygame.font.SysFont("Comic Sans MS", 30)
  H_fonte = pygame.font.Font("fonte/Kenney_Future_Narrow.ttf",30)
  Easteregg = pygame.transform.scale(pygame.image.load("pics2/star.png"),(35, 35))

  pygame.mixer.music.load("sounds/sound_track2.wav")
  pygame.mixer.music.play(-1)
  pygame.mixer.music.set_volume(0.1)

  # main loop
  while not game_over:
    # mouse current position
    cur = pygame.mouse.get_pos()
    print(cur)

    # set bg
    if bgi==0:
     screen.blit(bg , (0,-2))
    if bgi==1:
     screen.blit(bg2 ,(0,-2))
     screen.blit(board,(60,80))
     screen.blit(starman,(580, 195))
     #tava pensando em textbubble mudar quando clica
     screen.blit(textbubble,(445,150))
     screen.blit(planeta1,(250,135))
     screen.blit(bluesquare,(190,280))
     screen.blit(bluesquare,(240,280))
     screen.blit(bluesquare,(290,280))
     screen.blit(bluesquare,(340,280))
     screen.blit(bluesquare,(390,280))

     # screen.blit(bg3, (0,-2))
    #tirei, antes ficava o screen.blit bg aki
    #screen.blit(planeta1, (360,230))
    #screen.blit(planeta2,(550,300))


    # Travel state
    if gs == 0:
      score=highscore1+highscore2
      screen.blit(planeta1, (360,230))
      screen.blit(planeta2,(550,300))
      if checkstage==1:
        screen.blit(check, (380,240))
      if checkstage2==1:
         screen.blit(check2,(567,308))
      ship.draw_ship(cur, screen)
      dt = clock.tick()
      if 65**2 +40 > (420 - int(ship.m[0]))**2 + (296 - int(ship.m[1]))**2:
        if once==0:
         once=1
         print("a")
         pygame.mixer.Sound.play(enterlevel)
         gs=2
      if 38**2 +25 > (587 - int(ship.m[0]))**2 + (337 - int(ship.m[1]))**2:
         if once==0:
          once=1
          print("a")
          pygame.mixer.Sound.play(enterlevel)
          gs=5
      # planeta1circle=pygame.draw.rect(screen, red,planeta1)
      #if planeta1.detect_colision(ship):
        #     print("opa")
      # event handling, gets all events from the event queue
      if pygame.mouse.get_pressed()[0]==1:
        print(pygame.mouse.get_rel())
        ship.move_ship(cur, screen)
      for event in pygame.event.get():
        # if quiting the game
        if event.type == pygame.QUIT:
          game_over = True

      # draw
          ship.draw_ship(cur, screen)

      score_label = myfont.render("score: "+str(score), 3, (255, 255, 0))
      screen.blit(score_label, (10, 10))

      if ship.v>0:
        ship.v-=1


      # generate Easter egg
     # if random.randint(0, 25000) <= 1:
    #    print("Easteregg generated")
    #    new_lifes.append(Easteregg(width_screen, height_screen))


      # update every life status
      for l in new_lifes:
        l.update_life(screen)
        if l.pos[0] >= width_screen or l.pos[1] >= height_screen:
          new_lifes.remove(l)
        if l.pos[0] <= -10 or l.pos[1] <= -10:
          new_lifes.remove(l)

        if l.detect_colision(ship.points[0]):
         # q q easter egg vai fazer?


#        for b in bullets:
#          if a.detect_colision(b.pos[1]):
#            asteroids.remove(a)
#            bullets.remove(b)
            #Vou fazer a colisão entre o ship e a terra aki
          for p in ship.points:
            if a.detect_colision(p):
              invincible = True
              ship.lifes -= 1
              if ship.lifes <= 0:
                # change game state to dead
                gs = 2
                print("G A M E  O V E R")

      # update every satellite status
      for s in satellites:
        s.update_satellite(screen)
        if s.pos[0] >= width_screen or s.pos[1] >= height_screen:
          satellites.remove(s)

        for p in ship.points: # colisão com satelite
          if s.detect_colision(p):
            gs = 3
            flag = True
            print("S A T E L L I T E")
            satellites.remove(s)

    #  display_hearts(ship.lifes, heart_img, screen)

    # pause state
    elif gs == 1:
#O pause state deve retornar pro gs especifico e não pro gs=0
#Eu tirei pq n vi muita utilidade
      pause_label = myfont.render("Pause", 3, (255, 255, 0))
      label1 = myfont.render("Press q to quit", 3, (255, 255, 0))
      label2 = myfont.render("Press p to unpause", 3, (255, 255, 0))


      screen.blit(pause_label, (width_screen/2 - 20, height_screen/2 - 10))
      screen.blit(label1, (width_screen/2 - 60, height_screen/2 + 30))
      screen.blit(label2, (width_screen/2 - 80, height_screen/2 + 50))

      for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
          if event.key == pygame.K_p:
            gs = 0

          if event.key == pygame.K_q:
            game_over = True

    # game1 state
    elif gs == 2:
       if once1==0:
           bgi=1
           once1=once1+1
           pygame.display.update()
           screen.blit(bg2, (0,-2))
       timer=timer-0.0333333
       x2=str(int(timer))
     #score_labelV2=myfont.render("Sua Pontuação foi "+ str(score), 3, (255,255, 0))
       Time_label = myfont.render("Time: " + x2, 3, (0, 0, 0))
          #screen.blit(T, (120, 400))
       if once2==0:
          x=ordem(ordemlist)
          once2=once2+1
        #ordem vai pegar uma lista com as letras arrumadas de forma aleatória
       l1=x[0]
       #l1 pode ser qualquer letra, aí vai pegar a letra e criar ret1 com ela e o msm com os outros
       ret1 = letter_images[l1].get_rect(topleft=(pos1x,pos1y))
       screen.blit(letter_images[l1],ret1)
       l2=x[1]
       ret2 = letter_images[l2].get_rect(topleft=(pos2x,pos2y))
       screen.blit(letter_images[l2], ret2)
       l3=x[2]
       ret3 = letter_images[l3].get_rect(topleft=(pos3x,pos3y))
       screen.blit(letter_images[l3], ret3)
       l4=x[3]
       ret4 = letter_images[l4].get_rect(topleft=(pos4x,pos4y))
       screen.blit(letter_images[l4], ret4)
       l5=x[4]
       ret5 = letter_images[l5].get_rect(topleft=(pos5x,pos5y))
       screen.blit(letter_images[l5], ret5)

      #label1 = myfont.render("Press q to quit", 3, (255, 255, 0))
      #label2 = myfont.render("Press enter to play again", 3, (255, 255, 0))
       # Isso aki é o timer lá emcima
       screen.blit(Time_label, (600 , 25))
       if pygame.mouse.get_pressed()[0]==1:
           #Botei o once3 pra ser 0 e o clique inicial pra vc n ter q só arrastar segurando até a letra e levar
           if once3==0:
             cliqueinicial=pygame.mouse.get_pos()
             once3=1
           if ret1.collidepoint(cliqueinicial) and umsó=="false":
               test1=1
               umsó="true"
           if test1==1:
            pos1x=cur[0]
            pos1y=cur[1]
           if ret2.collidepoint(cliqueinicial) and umsó=="false":
               umsó="true"
               test2=1
           if test2==1:
            pos2x=cur[0]
            pos2y=cur[1]
           if ret3.collidepoint(cliqueinicial) and umsó=="false":
                test3=1
                umsó="true"
           if test3==1:
             pos3x=cur[0]
             pos3y=cur[1]
           if ret4.collidepoint(cliqueinicial) and umsó=="false":
                test4=1
                umsó="true"
           if test4==1:
             pos4x=cur[0]
             pos4y=cur[1]
           if ret5.collidepoint(cliqueinicial) and umsó=="false":
                test5=1
                umsó="true"
           if test5==1:
             pos5x=cur[0]
             pos5y=cur[1]
         #  print(cur)
       if pygame.mouse.get_pressed()[0]==0:
           #Quando soltar o botão vai pegar outro clique inicial se clicar dnv e vai desativar o movimento
           once3=0
           test2=0
           test1=0
           test3=0
           test4=0
           test5=0
           umsó="false"
           listaposx=[pos1x,pos2x,pos3x,pos4x,pos5x]
           listaposy=[pos1y,pos2y,pos3y,pos4y,pos5y]
           #print(listaposx)
         #  for x in range(-1,6) and y in range(5,10):
         #  for p in listaposx and p2 in listaposy:

           if pos1x in range(179,194) and pos1y in range(261,296):
               pos1x=187
               pos1y=280
           if pos1x in range(230,244) and pos1y in range(261,296):
               pos1x=237
               pos1y=280
           if pos1x in range(280,294) and pos1y in range(261,296):
               pos1x=287
               pos1y=280
           if pos1x in range(330,344) and pos1y in range(261,296):
               pos1x=337
               pos1y=280
           if pos1x in range(379,395) and pos1y in range(261,296):
               pos1x=387
               pos1y=280
           if pos2x in range(179,194) and pos2y in range(261,296):
               pos2x=187
               pos2y=280
           if pos2x in range(230,244) and pos2y in range(261,296):
               pos2x=237
               pos2y=280
           if pos2x in range(280,294) and pos2y in range(261,296):
               pos2x=287
               pos2y=280
           if pos2x in range(330,344) and pos2y in range(261,296):
               pos2x=337
               pos2y=280
           if pos2x in range(379,395) and pos2y in range(261,296):
               pos2x=387
               pos2y=280
           if pos3x in range(179,194) and pos3y in range(261,296):
               pos3x=187
               pos3y=280
           if pos3x in range(230,244) and pos3y in range(261,296):
               pos3x=237
               pos3y=280
           if pos3x in range(280,294) and pos3y in range(261,296):
               pos3x=287
               pos3y=280
           if pos3x in range(330,344) and pos3y in range(261,296):
               pos3x=337
               pos3y=280
           if pos3x in range(379,395) and pos3y in range(261,296):
               pos3x=387
               pos3y=280
           if pos4x in range(179,194) and pos4y in range(261,296):
               pos4x=187
               pos4y=280
           if pos4x in range(230,244) and pos4y in range(261,296):
               pos4x=237
               pos4y=280
           if pos4x in range(280,294) and pos4y in range(261,296):
               pos4x=287
               pos4y=280
           if pos4x in range(330,344) and pos4y in range(261,296):
               pos4x=337
               pos4y=280
           if pos4x in range(379,395) and pos4y in range(261,296):
               pos4x=387
               pos4y=280
           if pos5x in range(179,194) and pos5y in range(261,296):
               pos5x=187
               pos5y=280
           if pos5x in range(230,244) and pos5y in range(261,296):
               pos5x=237
               pos5y=280
           if pos5x in range(280,294) and pos5y in range(261,296):
               pos5x=287
               pos5y=280
           if pos5x in range(330,344) and pos5y in range(261,296):
               pos5x=337
               pos5y=280
           if pos5x in range(379,395) and pos5y in range(261,296):
               pos5x=387
               pos5y=280
           listafinal=[str(l1),str(l2),str(l3),str(l4),str(l5)]
         #  print(listafinal)
           TERRA=["T","E","R","R","A"]
           #print(listafinal)
           for xx in range(0,5):
               if listafinal[xx]=="T":
                  x1=xx+1
               if listafinal[xx]=="E":
                  x2=xx+1
               if listafinal[xx]=="R" and firstr==0:
                  firstr=1
                  x3=xx+1
               if listafinal[xx]=="R" and firstr==1:
                  x4=xx+1
               if listafinal[xx]=="A":
                  x5=xx+1
           listaop=[x1,x2,x3,x4,x5]
         #  print(listaop)

           for xx in range(0,5):
            for xyz in range(0,5):
             if TERRA[xyz] == listafinal[xx] and xyz<5 and once4==0:
                 varia=xyz+1
                 listaposcerta.append(varia)
                 #print(*listaposcerta)
           if once4==0:
            for variavelinutil in range(0,7):
              listaposcerta2.append(listaposcerta[variavelinutil])
            listaposcerta2.remove(3)
            once4=1
            for number in range(5,0, -1):
             if listaposcerta2[number]==4:
              listaposcerta2.pop(number)
              break
           if once5==0 and once4==1:
            listaposcerta.remove(4)
            print(listaposcerta)
            for number in range(5, 0, -1):
             if listaposcerta[number]==3:
              listaposcerta.pop(number)
              break
            once5=1
          # print(listaposcerta)
          #print(listaposcerta2)
           lugarescertos=0
           for xy in range(0,5):
            posição=int(listaposcerta2[xy])
            if int(listaposx[xy])==137+(50*(posição)):
                 lugarescertos=lugarescertos+1
            else:
                if xy>0 and xy<4:
                     if int(listaposx[xy+1])==137+(50*(posição)) or int(listaposx[xy-1])==137+(50*(posição)):
                         lugarescertos=lugarescertos+1


           if lugarescertos!=5:
            lugarescertos=0
            for xy in range(0,5):
             posição=int(listaposcerta[xy])
             if int(listaposx[xy])==137+(50*(posição)):
                 lugarescertos=lugarescertos+1
           if lugarescertos==5:
             score1=int(int(timer)/5)*30
             if highscore1<=score1:
                highscore1=score1
             checkstage=1
             gs=0
             ship.lifes = 3
             ship.m = mid
             asteroids = []
             bullets = []
             satellites = []
             new_lifes = []
             once=0
             pos1x=120
             pos2x=200
             pos3x=280
             pos4x=360
             pos5x=440
             pos1y=400
             pos2y=400
             pos3y=400
             pos4y=400
             pos5y=400
             once1=0
             once2=0
             once4=0
             once5=0
             timer=300
             invincible = False
             teste="false"
             perrepetida="false"
             ship.color = (255,0,0)
             bgi=0
             firstr=0
             listaposcerta=[]
             listaposcerta2=[]



       for event in pygame.event.get():
        if event.type == pygame.QUIT:
          game_over = True
    #    while (event.type == pygame.MOUSEBUTTONDOWN):
    #      pos_m = pygame.mouse.get_pos()
        #  print(pos_m)

        if event.type == pygame.KEYDOWN:
            #aperta para quitar da fase e voltar pro menu
          if event.key == pygame.K_q:
            gs = 0
            ship.lifes = 3
            ship.m = mid
            asteroids = []
            bullets = []
            satellites = []
            new_lifes = []
            once=0
            pos1x=120
            pos2x=200
            pos3x=280
            pos4x=360
            pos5x=440
            pos1y=400
            pos2y=400
            pos3y=400
            pos4y=400
            pos5y=400
            once1=0
            once2=0
            once4=0
            once5=0
            timer=300
            invincible = False
            teste="false"
            perrepetida="false"
            score=0
            ship.color = (255,0,0)
            bgi=0
            firstr=0
            listaposcerta=[]
            listaposcerta2=[]
       if x2=="0":
            gs = 0
            ship.lifes = 3
            ship.m = mid
            pos1x=120
            pos2x=200
            pos3x=280
            pos4x=360
            pos5x=440
            pos1y=400
            pos2y=400
            pos3y=400
            pos4y=400
            pos5y=400
            asteroids = []
            bullets = []
            satellites = []
            new_lifes = []
            once=0
            timer=300
            invincible = False
            teste="false"
            perrepetida="false"
            score=0
            ship.color = (255,0,0)
            bgi=0
            once1=0
            once2=0
            once4=0
            once5=0
            firstr=0
            listaposcerta=[]
            listaposcerta2=[]



    # Satelite State
    elif gs == 3:
           if flag == True:
             per, resposta, perrepetida = perguntas()
             flag = False
             pergunta = myfont.render(per, 3, (255, 255, 0))

           #desenha_tela_satelite(pergunta, screen, height_screen, width_screen,myfont)
           Botao1, Botao2 =tela_satelite(screen, height_screen, width_screen, H_fonte,per)

           for event in pygame.event.get():
             tent = False
             if event.type == pygame.QUIT:
               game_over = True
             if (event.type == pygame.MOUSEBUTTONDOWN) or event.type == pygame.KEYDOWN:
               pos_m = pygame.mouse.get_pos()
               key = pygame.key.get_pressed()
               if (Botao1.collidepoint(pos_m)) or (key[pygame.K_y]):
                 print("Y")
                 aux = True
                 tent = True
               elif (Botao2.collidepoint(pos_m)) or (key[pygame.K_n]):
                 print("N")
                 aux = False
                 tent = True
               else:
                 print("nada")
               if tent:
                 if resposta == aux:
                   res = "ACERTOU!"
                   if teste != "true":
                    objetivo=objetivo+1
                    gs=5
                   if teste=="true":
                     tela_satelite(screen, height_screen, width_screen, H_fonte, per, res)
                     """"
                     screen.fill((0, 0, 0))
                     screen.blit(res, (width_screen / 2 - 20, height_screen / 2 + 10))
                     pygame.display.update()
                     time.sleep(2)
                     """
                     time.sleep(2)
                     gs=4
                 else:
                   res = "ERROU :("
                 tela_satelite(screen, height_screen, width_screen, H_fonte, per, res)
                 """"
                 screen.fill((0, 0, 0))
                 screen.blit(res, (width_screen / 2 - 20, height_screen / 2 + 10))
                 pygame.display.update()
                 time.sleep(2)
                 """
                 time.sleep(2)
                 if teste=="true":
                  gs = 4
                 if teste != "true":
                  gs = 5
    # Menu state
    elif gs == 4:
      b_start,b_dead,b_sat,b_saida = desenha_menu(screen, height_screen, width_screen,bg)
      for event in pygame.event.get():
        # if quiting the game
        if event.type == pygame.QUIT:
          game_over = True
        if (event.type == pygame.MOUSEBUTTONDOWN):
          pos_m = pygame.mouse.get_pos()
          if b_start.collidepoint(pos_m):
            perrepetida=[]
            teste="false"
            gs = 0
          elif b_dead.collidepoint(pos_m):
            gs = 2
          elif b_sat.collidepoint(pos_m):
            flag = True
            teste="true"
            gs=3
          elif b_saida.collidepoint(pos_m):
            game_over = True
      #gs = 0



    pygame.display.update()
    clock.tick(30)
    #Game2 state
    if gs == 5:
      bgi=2
      timer=timer-0.0333333
      x2=str(int(timer))
      Time_label = myfont.render("Time: " + x2, 3, (0, 0, 0))
      if x2=="0":
          gs = 0
          ship.lifes = 3
          ship.m = mid
          asteroids = []
          bullets = []
          satellites = []
          new_lifes = []
          once=0
          timer=300
          invincible = False
          teste="false"
          perrepetida="false"
          score=0
          ship.color = (255,0,0)
          bgi=0
          objetivo=0
      if bgi==2:
        screen.blit(bg3, (0,-2))
        screen.blit(Time_label, (600 , 25))
      if objetivo==3:
          score2=int(int(timer)/5)*30
          if highscore2<=score2:
             highscore2=score2
          checkstage2=1
          gs=0
          ship.lifes = 3
          ship.m = mid
          asteroids = []
          bullets = []
          satellites = []
          new_lifes = []
          once=0
          timer=300
          invincible = False
          teste="false"
          perrepetida="false"
          ship.color = (255,0,0)
          bgi=0
          objetivo=0

      if pygame.mouse.get_pressed()[0]==1:
          print(pygame.mouse.get_rel())
          ship.move_ship(cur, screen)

      # event handling, gets all events from the event queue
      if pygame.mouse.get_pressed()[0]==1:
          ship.move_ship(cur, screen)
      for event in pygame.event.get():
        # if quiting the game
        if event.type == pygame.QUIT:
          game_over = True
        if event.type == pygame.KEYDOWN:
            #aperta para quitar da fase e voltar pro menu
          if event.key == pygame.K_q:
            gs = 0
            ship.lifes = 3
            ship.m = mid
            asteroids = []
            bullets = []
            satellites = []
            new_lifes = []
            once=0
            timer=300
            invincible = False
            teste="false"
            perrepetida="false"
            score=0
            ship.color = (255,0,0)
            bgi=0
            firstr=0
            listaposcerta=[]
            listaposcerta2=[]
            objetivo=0

        if event.type == pygame.KEYDOWN:
          if event.key == pygame.K_SPACE:
            # play shooting sound
            pygame.mixer.Sound.play(shot_sound)

            # generate new bullet every time space is pressed and add it to bullet list
            new_bullet = Bullet(len_bullet, vel_bullet)
            new_bullet.gen_bullet(ship, cur, screen)

            bullets.append(new_bullet)

      # draw
      if not invincible:
        ship.draw_ship(cur, screen)
      else:
        count-=1
        if xx:
          ship.draw_ship(cur, screen)
        xx = not xx
        if count == 0:
          count = 60
          invincible = False

      score_label = myfont.render("Satélites: "+str(objetivo)+"/3", 3, (255, 255, 0))
      screen.blit(score_label, (10, 10))

      if ship.v>0:
        ship.v-=1

      # generate asteroid
      if random.randint(0, 100) < 7:
        asteroids.append(Asteroid(width_screen, height_screen))

      # generate satellite
      if random.randint(0, 300) == 0:
        satellites.append(Satellite(width_screen, height_screen))

      # generate new life
      if random.randint(0, 1000) <= 1:
        print("new life generated")
        new_lifes.append(Life(width_screen, height_screen))

      if len(bullets) > max_bullets:
        bullets.clear()

      # update every life status
      for l in new_lifes:
        l.update_life(screen)
        if l.pos[0] >= width_screen or l.pos[1] >= height_screen:
          new_lifes.remove(l)
        if l.pos[0] <= -10 or l.pos[1] <= -10:
          new_lifes.remove(l)

        if l.detect_colision(ship.points[0]):
         if ship.lifes <= 4:
          ship.lifes += 1
          new_lifes.remove(l)

      # update every bullet status
      for b in bullets:
        b.update_bullet(screen)
        if b.pos[0][0] >= width_screen or b.pos[0][1] >= height_screen:
          bullets.remove(b)

      # update every asteroid status
      for a in asteroids:
        a.update_asteroid(screen)
        if a.pos[0] >= width_screen or a.pos[1] >= height_screen:
          asteroids.remove(a)
        if a.pos[0] <= 0 or a.pos[1] <= 0:
          asteroids.remove(a)

        for b in bullets:
          if a.detect_colision(b.pos[1]):
            asteroids.remove(a)
            bullets.remove(b)
        if not invincible:
          for p in ship.points:
            if a.detect_colision(p):
              invincible = True
              ship.lifes -= 1
              if ship.lifes <= 0:
                # change game state to dead
                gs = 0
                ship.lifes = 3
                ship.m = mid
                asteroids = []
                bullets = []
                satellites = []
                new_lifes = []
                once=0
                timer=300
                invincible = False
                teste="false"
                perrepetida="false"
                score=0
                ship.color = (255,0,0)
                bgi=0
                objetivo=0

      # update every satellite status
      for s in satellites:
        s.update_satellite(screen)
        if s.pos[0] >= width_screen or s.pos[1] >= height_screen:
          satellites.remove(s)

        for p in ship.points: # colisão com satelite
          if s.detect_colision(p):
            gs = 3
            flag = True
            print("S A T E L L I T E")
            satellites.remove(s)

      display_hearts(ship.lifes, heart_img, screen)


if __name__=="__main__":
  main()
