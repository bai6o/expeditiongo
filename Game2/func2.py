import os.path

import pygame
import math
import random

black = (0, 0, 0)
white = (255, 255, 255)
red = (255, 0, 0)
blue = (0, 0, 255)
green = (0, 255, 0)
yellow = (255, 255, 0)
brown = (139, 69, 19)
maxSpeed = 15
perrepetida=[]


class Ship:
  def __init__(self, mid, height, velocity, lifes, color):
    self.m = mid
    self.h = height
    self.v = velocity
    self.lifes = lifes
    self.points = [[0, 0], [0, 0], [0, 0]]
    self.color = color

    self.img = pygame.transform.scale(pygame.image.load("pics2/ship.png"), (30, 30))

  def draw_ship(self, cur, screen):
    curx, cury = cur
    mx, my = self.m
    h = self.h

    # distance from m point to cursor
    mcur = math.sqrt((mx-curx)**2 + (my-cury)**2)

    t = h/mcur

    # front of ship
    px, py = [((1-t)*mx + t*curx), ((1-t)*my + t*cury)]

    # [UPDATE ANGLE]
    dy = cury - py
    dx = curx - px

    if curx-px != 0:
      s = dy / dx
    else:
      s = dy / 0.01

    ang = math.atan(s)

    if mx < curx:
      ang+=math.pi

    # side lenght
    l = h/math.cos(math.pi/6)

    ax = px + l*math.cos(ang-math.pi/6)
    ay = py + l*math.sin(ang-math.pi/6)

    bx = px + l*math.cos(ang+math.pi/6)
    by = py + l*math.sin(ang+math.pi/6)

    self.points = [[px, py], [ax, ay], [bx, by]]
    #pygame.draw.polygon(screen, self.color, self.points, 3)

    ship_x, ship_y = [(l*px +l*ax + l*bx)/(3*l) - l/2,\
                      (l*py +l*ay + l*by)/(3*l) - h/2]

    screen.blit(pygame.transform.rotate(self.img,\
              -(ang*180/math.pi)+90), (ship_x, ship_y))

    #pygame.transform.rotate()

  def move_ship(self, cur, screen):
    mx, my = self.m
    px, py = self.points[0]
    curx, cury = cur

    dy = cury - my
    dx = curx - mx

    if curx-mx != 0:
      a = dy / dx
    else:
      a = dy / 0.01

    ang = math.atan(a)
    if mx>curx:
      ang+=math.pi

    mcur = math.sqrt((mx-curx)**2 + (my-cury)**2)
    if mcur < 100:
      self.v = 6
    else:
      self.v = 10

    dxx = abs(px-mx)/self.h
    if dxx>1:
      dxx -= int(dxx)
    elif dxx<-1:
      dxx += int(dxx)

    dyy = abs(py-my)/self.h
    if dyy>1:
      dyy -= int(dyy)
    elif dyy-1:
      dyy += int(dyy)

    mx += self.v*math.cos(ang + math.acos(dxx))
    my += self.v*math.sin(ang + math.asin(dyy))

    self.m = [mx, my]

class Bullet:
  def __init__(self, l, vel):
    self.pos = [[0,0],[0,0]]
    self.l = l
    self.v = vel
    self.ang = 0
    self.img = pygame.transform.scale(pygame.image.load("pics2/bullet.png"),\
               (15, 30))

  def gen_bullet(self, ship, cur, screen):
    print("piu piu")
    curx,cury = cur
    mx, my = ship.m
    h = ship.h

    # distance from m point to cursor
    mcur = math.sqrt((mx-curx)**2 + (my-cury)**2)

    t = h/mcur

    # front of bullet
    px, py = [((1-t)*mx + t*curx), ((1-t)*my + t*cury)]

    # [UPDATE ANGLE]
    dy = cury - py
    dx = curx - px

    if curx-px != 0:
      s = dy / dx
    else:
      s = dy / 0.01

    ang = math.atan(s)

    if mx>curx:
      ang+=math.pi

    bx = px + self.l*math.cos(ang)
    by = py + self.l*math.sin(ang)

    self.pos = [[px, py], [bx, by]]
    self.ang = ang


  def update_bullet(self, screen):
    ix, iy = self.pos[0]
    ex, ey = self.pos[1]

    #pygame.draw.line(screen, yellow, [ix, iy], [ex, ey], 2)

    new_ix = ix + self.v*math.cos(self.ang)
    new_iy = iy + self.v*math.sin(self.ang)

    new_ex = new_ix + self.l*math.cos(self.ang)
    new_ey = new_iy + self.l*math.sin(self.ang)

    self.pos = [[new_ix, new_iy], [new_ex, new_ey]]

    bx, by = [(self.pos[0][0]+self.pos[1][0])/2 -7,\
               (self.pos[0][1]+self.pos[1][1])/2 -7]

    sprite_ang = -(self.ang*180/math.pi)-90
    #print(-sprite_ang)

    screen.blit(pygame.transform.rotate(self.img,\
              -(self.ang*180/math.pi)-90), (bx, by))

class Asteroid:
  def __init__(self, w, h):
    self.r = random.randint(15, 40)
    self.v = random.randint(2, 8)
    # self.ang = random.uniform(0, 2*math.pi)

    r = random.randint(0, 10)
    if r%2==0:
      x = random.randint(0, w)
      y = random.choice([0,h])
      if y==0:
        self.ang = math.pi/2
      else:
        self.ang = -math.pi/2
    else:
      y = random.randint(0, h)
      x = random.choice([0,w])
      if x==0:
        self.ang = 0
      else:
        self.ang = math.pi

    self.pos = [x, y]

    if random.randint(0,100) > 50:
      self.img  = pygame.image.load("pics2/meteor1.png")
    else:
      self.img  = pygame.image.load("pics2/meteor2.png")

    self.img = pygame.transform.scale(self.img, (self.r*2, self.r*2))

  def update_asteroid(self, screen):
    new_x = self.pos[0] + self.v*math.cos(self.ang)
    new_y = self.pos[1] + self.v*math.sin(self.ang)

    #pygame.draw.circle(screen, brown, [new_x, new_y], self.r, 10)

    blit_x = new_x - self.r
    blit_y = new_y - self.r

    screen.blit(self.img, (blit_x, blit_y))

    self.pos = [new_x, new_y]

  def detect_colision(self, xy):
    x, y = xy
    if ((x-self.pos[0])**2 + (y-self.pos[1])**2) < self.r**2:
      return True
    else:
      return False

class Satellite:
    def __init__(self, w, h):
      self.r = 25
      self.v = random.randint(2, 8)
      self.ang = random.uniform(0, 2*math.pi)

      r = random.randint(0, 10)
      if r%2==0:
        x = random.randint(0, w)
        y = random.choice([0,h])
        if y==0:
          self.ang = math.pi/2
        else:
          self.ang = -math.pi/2
      else:
        y = random.randint(0, h)
        x = random.choice([0,w])
        if x==0:
          self.ang = 0
        else:
          self.ang = math.pi

      self.pos = [x, y]
      self.img = pygame.transform.scale(pygame.image.load("pics2/satellite.png"), (30, 30))


    def update_satellite(self, screen):
      new_x = self.pos[0] + self.v*math.cos(self.ang)
      new_y = self.pos[1] + self.v*math.sin(self.ang)

      blit_x = new_x - self.r
      blit_y = new_y - self.r

      #pygame.draw.circle(screen, green, [new_x, new_y], self.r, 10)
      screen.blit(self.img, (blit_x, blit_y))
      self.pos = [new_x, new_y]

    def detect_colision(self, xy):
      x, y = xy
      if ((x-self.pos[0])**2 + (y-self.pos[1])**2) < self.r**2:
        return True
      else:
        return False

class Life:
  def __init__(self, w, h):
    self.r = 15
    self.v = random.randint(2, 8)

    r = random.randint(0, 10)
    if r%2==0:
      x = random.randint(0, w)
      y = random.choice([0,h])
      if y==0:
        self.ang = math.pi/2
      else:
        self.ang = -math.pi/2
    else:
      y = random.randint(0, h)
      x = random.choice([0,w])
      if x==0:
        self.ang = 0
      else:
        self.ang = math.pi

    self.pos = [x, y]

    self.img = pygame.transform.scale(pygame.image.load("pics2/heart.png"),(30, 30))

  def update_life(self, screen):
    new_x = self.pos[0] + self.v*math.cos(self.ang)
    new_y = self.pos[1] + self.v*math.sin(self.ang)

    #pygame.draw.circle(screen, brown, [new_x, new_y], self.r, 10)

    blit_x = new_x - self.r
    blit_y = new_y - self.r

    screen.blit(self.img, (blit_x, blit_y))

    self.pos = [new_x, new_y]

  def detect_colision(self, xy):
    x, y = xy
    if ((x-self.pos[0])**2 + (y-self.pos[1])**2) < self.r**2:
      return True
    else:
      return False

def display_hearts(num_hearts, img, screen):
  x = 10
  y = 550
  for i in range(num_hearts):
    screen.blit(img, (x, y))
    x += 35

def perguntas():
  i = random.randint(1,4)
  switch = {
    1:("Uma supernova é morte de estrela?",True),
    2:("Buraco negro não tem massa?",False),
    3:("O sol é uma estrela?",True),
    4:(" Júpiter é maior que a Terra?", True)
  }
  pergunta, resposta = switch.get(i,("sem pergunta",False))
  return (pergunta,resposta,perrepetida)

def desenha_tela_satelite(pergunta,screen,height_screen,width_screen,myfont,H_fonte):
  BLUE = (0, 0, 150)
  Tela_s = pygame.Rect((50,50), (width_screen-100,height_screen-100)) # faz um retangulo do tamanho da tela da pergunta
  pygame.draw.rect(screen, (169, 169, 169), Tela_s) # fundo cinza claro
  pygame.draw.rect(screen, (153, 51, 153), Tela_s, 10) # borda roxa
  Psize = pergunta.get_size() # tamanho da pergunta
  texto = "Teste rotineiro da ANEB"
  t = H_fonte.render(texto, 3, BLUE)
  screen.blit(t,(100,100))
  texto = "Responda a pergunta:"
  t = H_fonte.render(texto, 3, BLUE)
  screen.blit(t, (100, 100 + t.get_height()))
  P_r = pygame.Rect(((width_screen-Psize[0])/2, height_screen / 2 - 10), (Psize[0]+5,Psize[1]+5))
  res = myfont.render("Y/N", 3, (255, 255, 0))
  pygame.draw.rect(screen, (128, 128, 128), P_r)
  pygame.draw.rect(screen, (0, 0, 0), P_r,3)
  screen.blit(pergunta, ((width_screen-Psize[0])/2, height_screen / 2 - 10))
  screen.blit(res, (width_screen / 2, height_screen / 2 + 30))
  pygame.display.update()

def tela_satelite(screen, height_screen, width_screen, H_fonte, per,resposta = ""):
  BLUE = (0, 0, 150)
  DARK_GREEN = (0, 100, 0)
  LIME_GREEN = (50, 205, 50)
  fonte_tela = pygame.font.SysFont("Times New Roman",28)
  Perg = fonte_tela.render(per, True, LIME_GREEN)

  fundo_metal=pygame.transform.scale(pygame.image.load(os.path.join('pics2', 'Fundo_metalico.png')).convert_alpha()
                                     , (width_screen-100, height_screen-100))
  screen.blit(fundo_metal, (50, 50))
  ANEB_logo = pygame.transform.scale(pygame.image.load(os.path.join('pics2','ANEB.png')).convert_alpha()
                                     , (80, 80))
  screen.blit(ANEB_logo,(fundo_metal.get_width()-ANEB_logo.get_width(),100))
  sim = pygame.transform.scale(pygame.image.load(os.path.join('pics2', 'sim.png')).convert_alpha()
                               , (80, 80))

  nao = pygame.transform.scale(pygame.image.load(os.path.join('pics2', 'nao.png')).convert_alpha()
                               , (80, 80))
  b_sim = pygame.Rect( ((width_screen/2) - sim.get_width(), height_screen - 180), sim.get_size() )
  b_nao = pygame.Rect( ((width_screen/2) + sim.get_width(), height_screen - 180), nao.get_size() )

  screen.blit(sim,b_sim)
  screen.blit(nao,b_nao)

  texto = "Teste rotineiro da ANEB"
  t = H_fonte.render(texto, 3, BLUE)
  P_ini = (width_screen*0.7 - t.get_width(), 100)
  screen.blit(t,P_ini)
  texto = "Responda a pergunta:"
  t = H_fonte.render(texto, 3, BLUE)
  screen.blit(t,(P_ini[0],P_ini[1] + t.get_height()))

  Psize = Perg.get_size()  # tamanho da pergunta
  P_r = pygame.Rect((P_ini[0],P_ini[1] + 2*t.get_height()+30),
                    (fundo_metal.get_width()-P_ini[0]-100, (fundo_metal.get_width()/2)-nao.get_height()-50))
  pygame.draw.rect(screen, DARK_GREEN, P_r)
  pygame.draw.rect(screen, (0, 0, 0), P_r, 3)
  if resposta == "":
    screen.blit(Perg, (P_ini[0],P_ini[1] + 2*t.get_height()+30))
  else:
    resi = fonte_tela.render(resposta, True, LIME_GREEN)
    screen.blit(resi, (P_ini[0], P_ini[1] + 2 * t.get_height() + 30))

  pygame.display.update()
  return b_sim, b_nao

def desenha_menu(screen,height_screen,width_screen,bg):
  #print("entrou na tela")
  BLUE = (0, 0, 150)
  ROXO = (148,0,211)
  BLACK = (0,0,0)
  YELLOW = (255,255,0)

  times = pygame.font.SysFont("Times new roman",50)
  times = pygame.font.Font("fonte/Kenney_Future_Narrow.ttf",50)
  screen.blit(bg, (0, -2))
  fundo_metal = pygame.transform.scale(pygame.image.load(os.path.join('pics2', 'Fundo_metalico.png')).convert_alpha()
                                       , (round(width_screen*0.8),round(height_screen*0.8)))
  logo = pygame.transform.scale(pygame.image.load(os.path.join('pics3', 'Logo.png')).convert_alpha()
                                       , (200, 200))
  screen.blit(fundo_metal, (width_screen*0.1,height_screen*0.1))
  screen.blit(logo, (width_screen*0.6, height_screen*0.2))
  titulo = times.render("Expedition",True,BLUE)
  screen.blit(titulo,(width_screen*0.2,height_screen*0.2))
  times = pygame.font.Font("fonte/Kenney_Future_Narrow.ttf",60)
  titulo2 = times.render("GO!", True, ROXO)
  screen.blit(titulo2, (width_screen * 0.2, height_screen * 0.2+titulo.get_height()+5))

  pos_y = height_screen * 0.2+titulo.get_height()+5 + titulo2.get_height() # posição vertical apos o titulp
  pos_x = width_screen * 0.2
  botao_size = (300,60)

  nao = pygame.transform.scale(pygame.image.load(os.path.join('pics', 'nao.png')).convert_alpha()
                               , (80, 80))
  b_nao = pygame.Rect((width_screen * 0.7,height_screen*0.85 - nao.get_height()), nao.get_size())

  screen.blit(nao,b_nao)


  b_start = pygame.Rect((pos_x,pos_y+5),botao_size)
  b_dead = pygame.Rect((pos_x,pos_y + botao_size[1]+5+5),botao_size)
  b_satelite = pygame.Rect((pos_x,pos_y + 2*(botao_size[1]+5)+5),botao_size)

  pygame.draw.rect(screen, BLACK, b_start)
  start = times.render("START",True,YELLOW)
  screen.blit(start,b_start)
  pygame.draw.rect(screen, BLACK, b_dead)
  dead = times.render("DEAD", True, YELLOW)
  screen.blit(dead, b_dead)
  pygame.draw.rect(screen, BLACK, b_satelite)
  sat = times.render("SATELITE", True, YELLOW)
  screen.blit(sat, b_satelite)


  return b_start,b_dead,b_satelite,b_nao

def ordem(ordemlist):
    a1 = "R"
    a2 = "R"
    a3 = "A"
    a4 = "E"
    a5 = "T"
    number=4
    ordemlist=[]
    lista=[a1, a2,a3,a4,a5]
    while number>=0:
     r=random.randint(0,number )
     x1=lista[r]
     while r==4 and x1=="T":
      r=random.randint(0,number)
     x1=lista[r]
     lista.remove(x1)
     ordemlist.append(x1)
     number=number-1
    #print(ordemlist)
    return ordemlist
